#!/bin/env bash
set -x
URL_FILE="$1"
CHANNEL_NAME=""
CHANNEL_DIR=""
DATE_NOW=`date +"%Y%m%d_%H%M%S"`

# redirect all three of stdin, stdout and stderr (not just stdout!)
exec >"/var/log/filepuller/txt-$date_now-$$.txt" 2>"/var/log/filepuller/err-$date_now-$$.txt" </dev/null

# ignore any SIGHUP signals received
trap : SIGHUP


createChannelDir() {
	CHANNEL_NAME=$(yt-dlp -v --proxy http://127.0.0.1:1337 --skip-download --print "%(uploader)s" "$line" | tr -d '[:space:]' &)
	mkdir /jellyfin/tmp/downloaded/$CHANNEL_NAME 
	cd /jellyfin/tmp/downloaded/$CHANNEL_NAME || exit 1 
	echo "$CHANNEL_NAME"
}

processChannel() {
	yt-dlp --write-description --write-comments --write-annotations --write-sub --write-thumbnail --write-info-json --sponsorblock-remove all -f bestvideo[height\<=720]+bestaudio/best[height\<=720] --embed-thumbnail --embed-subs  --embed-chapters --concurrent-fragments 12 --proxy http://127.0.0.1:1337 --downloader=aria2c "$line" &
}

# Ensure the file actually exists...
if [ ! -f "$URL_FILE" ]; then
	echo "URL file not found: $URL_FILE"
	exit 1
fi

# Read the URL file...
while IFS= read -r line; do
	# Skip the empty lines
	if [[ -z "$line" ]]; then
		continue
	fi

	# If the current line is a video...
	if [[ $line == *"/watch"* ]]; then
		# Reset old channel name value
		CHANNEL_NAME=""
		# Reset old channel dir value
		CHANNEL_DIR=""
		# Get new channel dir value
		createChannelDir "$line" 
		#CHANNEL_DIR=$(createChannelDir "$line")
		#cd "$CHANNEL_DIR" || 
		#echo "Failed to cd into $CHANNEL_DIR, exiting..."; exit 1

	# If the current line is a channel...
	elif [[ $line == *"/channel/"* ]]; then
		# Process channel and begin downloading...
		processChannel "$line" 
	else 
		echo "Invalid URL format: $line"
	fi

	
done < "$URL_FILE"
