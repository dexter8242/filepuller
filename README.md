# Automated FilePuller for Jellyfin

This project introduces an automated script named `filepuller` for downloading videos or songs from YouTube channels using `yt-dlp` and organizing them into directories named after the channel. The script is designed to work seamlessly with Jellyfin, a popular media server software, to enhance your media library with content from YouTube channels.

## Features

- **Automatic Channel Directory Creation**: For each channel URL in the input file, the `filepuller` script creates a directory named after the channel.
- **Video and Song Download**: Downloads all videos or songs from the specified YouTube channels.
- **Persistent Running**: The script stays running even if the user logs out, ensuring continuous downloading.
- **Customizable**: Easily customize the download settings to suit your needs.
- **Privacy Focused**: This script is designed to use yt-dlp with a proxy.

## Defaults

By default, the song script downloads songs using these settings:
`-x` - Extracts the audio
`--write-description` - Writes the description
`--write-info-json` - Writes metadata information
`--proxy http://127.0.0.1:1337` - Forwards all traffic to local HTTP proxy in order to use it with a proxy. This can be adjusted to suit your needs.
`--embed-thumbnail` - In order to save the thumbnail
`--embed-subs` - In order to save subtitles
`--split-chapters` - In order to split each song (chapter) of i.e a mix into separate files.
`--concurrent-fragments 12` - In order to use parallel downloading.
`--downloader=aria2c` - In order to download multiple fragments at once.


## Prerequisites

- **yt-dlp**: A command-line program to download videos from YouTube.com and other video sites.
- **Bash**: The script is written in Bash, a command language interpreter for the GNU operating system.
- **Aria2c**: This script uses aria2c for parallel downloads.

## Installation

1. **Install yt-dlp**: Follow the installation instructions on the [yt-dlp GitHub page](https://github.com/yt-dlp/yt-dlp#installation) to install `yt-dlp` on your system.

2. **Download the `filepuller` Script**: Clone or download the `filepuller` script from this repository.

3. **Make the Script Executable**: Run the following command in your terminal to make the `filepuller` script executable:
   ```bash
   chmod +x filepuller.sh
   ```

4. **Prepare Your URL File**: Create a text file containing the URLs of the YouTube channels you wish to download from. Each URL should be on a new line. Place one URL to a video on the channel to download (this is required in order to grab the channel title to create the target download directory), for example:

```
  www.youtube.com/watch?=examplevideo
  www.youtube.com/channel/examplechannel
  
```

## Usage

To use the `filepuller` script, run the following command in your terminal:

```bash
./filepuller.sh /path/to/your/url_file.txt
```

Replace `/path/to/your/url_file.txt` with the actual path to your URL file.

## Configuration

The `filepuller` script is highly customizable. You can modify the script to change the download settings, such as video quality, audio format, and more. Refer to the `yt-dlp` documentation for a full list of options.

## Contributing

Contributions are welcome! If you have suggestions for improvements or find any bugs, please open an issue or submit a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Disclaimer

Please do not download copywritten content with this script, I will not be held responsible for any misuse of this script.
